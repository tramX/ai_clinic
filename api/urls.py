from rest_framework import routers

from api.views import TeamViewSet, PersonViewSet

router = routers.SimpleRouter()
router.register("teams", TeamViewSet)
router.register("persons", PersonViewSet)

urlpatterns = []
urlpatterns.extend(router.urls)
