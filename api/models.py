from django.db import models


class Team(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Person(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField()
    team = models.ForeignKey(to=Team, related_name="persons", on_delete=models.CASCADE)

    def __str__(self):
        return self.first_name
