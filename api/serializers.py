from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers

from api.models import Person, Team


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ("id", "first_name", "last_name", "email")


class TeamSerializer(WritableNestedModelSerializer):
    persons = PersonSerializer(many=True)

    class Meta:
        model = Team
        fields = ("id", "name", "persons")
